﻿using DemoAPI.Models.Data;
using DemoAPI.Models.Response;
using Newtonsoft.Json;

namespace DemoAPI.Service
{
    public class AddressService
    {
        public static AddressRespone GetAddresses(string villageId, List<Address> addresses)
        {
            var address = (from v in addresses
                           join c in addresses on v.ParentId equals c.Id
                           join d in addresses on c.ParentId equals d.Id
                           join p in addresses on d.ParentId equals p.Id
                           where v.Id == villageId
                           select new
                           {
                               VillageId = v.Id,
                               VillageName = v.NameKh,
                               CommuneId = c.Id,
                               CommuneName = c.NameKh,
                               DistrictId = d.Id,
                               DistrictName = d.NameKh,
                               ProvinceId = p.Id,
                               ProvinceName = p.NameKh
                           }).FirstOrDefault();
            var addressJson = JsonConvert.SerializeObject(address);
            var addressResponse = JsonConvert.DeserializeObject<AddressRespone>(addressJson);
            return addressResponse;
        }
    }
}
