﻿using DemoAPI.Models.Data;
using DemoAPI.Models.Request;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly DemoDbContext _context;

        public PositionController(DemoDbContext context)
        {
            _context = context;
        }

        [HttpGet("/api/positionList")]
        public async Task<ActionResult> GetPositionList()
        {
            try
            {
                var positionList = await _context.Positions.ToListAsync();

                if (positionList.FirstOrDefault() != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = "List of Position",
                        position = positionList
                    });
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "No data...!!!",
                        position = positionList
                    });
                }
                

            }catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message= ex.Message
                });
            }
        }

        [HttpGet("/api/getPositionById/{id}")]
        public async Task<ActionResult> GetPositionById(int id)
        {
            try
            {
                var positionList = await _context.Positions.Where(x => x.PositionId == id).ToListAsync();

                if (positionList.FirstOrDefault() != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = "",
                        position = positionList
                    });
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "position is not found...!!!"
                    });
                }
            }
            catch(Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message = ex.Message
                });
            }
        }

        [HttpPost("/api/position-cud")]
        public async Task<ActionResult>CUDPosition([FromBody] PositionRequest request)
        {
            try
            {
                var position = new Position();
                DateTime transectionDate = DateTime.Now;
                position.PositionId = request.PositionId;
                position.PositionNameKh = request.PositionNameKh;
                position.PositionNameEn = request.PositionNameEn;
                position.IsActive = request.IsActive;

                if(request.CUD == "C")
                {
                    position.PositionId = 0;
                    position.CreateBy = request.CreateBy;
                    position.CreateDate = transectionDate;
                    position.LastUpdateBy = request.CreateBy;
                    position.LastUpdateDate = transectionDate;

                    await _context.Positions.AddAsync(position);

                    _context.SaveChanges();

                    return Ok(new
                    {
                        status = "Success",
                        message = "Add new position sucessfully..."
                    });
                    

                }
                else if(request.CUD == "U")
                {
                    var existedPosition = await _context.Positions.Where(x => x.PositionId == request.PositionId).FirstOrDefaultAsync();
                    if(existedPosition != null)
                    {
                        _context.ChangeTracker.Clear();
                        position.PositionId = request.PositionId;
                        position.CreateBy = existedPosition.CreateBy;
                        position.CreateDate = existedPosition.CreateDate;

                        _context.Positions.Update(position);
                        _context.SaveChanges();


                        return Ok(new
                        {
                            status = "Success",
                            message = "Update successfully...!!!"
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            status = "Error",
                            message = "Data not found"
                        });
                    }
                }
                else if(request.CUD == "D")
                {
                    var existedPosition = await _context.Positions.Where(x => x.PositionId == request.PositionId).FirstOrDefaultAsync();
                    if(existedPosition != null)
                    {
                        _context.ChangeTracker.Clear();
                        _context.Positions.Remove(existedPosition);
                        _context.SaveChanges();

                        return Ok(new
                        {
                            status = "Success",
                            message = "Delete successfully...!!!"
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            status = "Error",
                            message = "Data is not found"
                        });
                    }
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "Invalid event"
                    });
                }

            }catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message = ex.Message
                });
            }

       
        }
    }
}
