﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MajorController : ControllerBase
    {
        private readonly DemoDbContext _context = new DemoDbContext();
        public MajorController(DemoDbContext context)
        {
            _context = context;
        }
        [HttpGet("/api/getMajors")]
        public async Task<ActionResult> GetMajors()
        {
            var major = await _context.Majors.ToListAsync();
            try
            {
                return Ok(new
                {
                    status = "Success",
                    message = "",
                    major = major,
                });
            }catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message = ex.Message,
                });
            }
        }
        
    }
}
