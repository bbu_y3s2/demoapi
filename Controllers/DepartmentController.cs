﻿using DemoAPI.Models.Data;
using DemoAPI.Models.Request;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        protected readonly DemoDbContext _context; //initial connection

        public DepartmentController(DemoDbContext context)
        {
            _context = context;
        }

        [HttpGet("/api/departmentList")]
        public async Task<ActionResult> GetDepartmentList()
        {
            try
            {
                var departmentList = await _context.Departments.ToListAsync();
                if(departmentList.FirstOrDefault() != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = "Department List",
                        department = departmentList

                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = "Error",
                        message = "No data",
                        department = departmentList
                    });
                }
               
            }catch (Exception ex)
            {
                return StatusCode(500,new
                {
                    status = "Error",
                    message = "Something went wrong.. "+ex.Message,
                });
            }
        }

        [HttpGet("/api/getDepartmentById/{id}")]
        public async Task<ActionResult> GetDepartmentById(int id)
        {
            try
            {
                var departmentList = await _context.Departments.Where(x => x.DepartmentId == id).ToListAsync();
                if(departmentList.FirstOrDefault() != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = "Departement Detail",
                        department = departmentList

                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = "Error",
                        message = "Department is not found...!!!"
                    });
                }
               

            }catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    status = "Error",
                    message = "Something went wrong...!!!" + ex.Message,
                });
            }
        }

        [HttpPost("/api/CUDDepartment")]
        public async Task<IActionResult>CUDDepartment([FromBody] DepartmentResquest req)
        {
            try
            {
                DateTime transectionDate = DateTime.Now;
                var department = new Department();
                department.DepartmentId = req.DepartmentId;
                department.DepartmentNameKh = req.DepartmentNameKh;
                department.DepartmentNameEn = req.DepartmentNameEn;
                department.IsActive = req.IsActive;
                if(req.CUD == "C")
                {
                    department.DepartmentId = 0;
                    department.CreateBy = req.CreateBy;
                    department.CreateDate = transectionDate;
                    department.LastUpdateBy = req.CreateBy;
                    department.LastUpdateDate = transectionDate;

                    await _context.Departments.AddAsync(department);
                    _context.SaveChanges();

                    return Ok(new
                    {
                        status = "Success",
                        message = "Add department successfully...!!!",

                    });
                }
                else if(req.CUD == "U")
                {
                    var existedDepartment = await _context.Departments.Where(x => x.DepartmentId == req.DepartmentId).FirstOrDefaultAsync();
                    if (existedDepartment != null)
                    {
                        _context.ChangeTracker.Clear();
                        department.DepartmentId = req.DepartmentId;
                        department.CreateBy = existedDepartment.CreateBy;
                        department.CreateDate = existedDepartment.CreateDate;

                        _context.Departments.Update(department);
                        await _context.SaveChangesAsync();

                        return Ok(new
                        {
                            status = "Success",
                            message = "Update successfuly...!!!",
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            status = "Error",
                            message = "Not found",
                        });
                    }
                }
                else if(req.CUD == "D")
                {
                    var existedDepartment = await _context.Departments.Where(x => x.DepartmentId == req.DepartmentId).FirstOrDefaultAsync();
                    if (existedDepartment != null)
                    {
                        _context.ChangeTracker.Clear();
                        _context.Departments.Remove(existedDepartment);
                        await _context.SaveChangesAsync();

                        return Ok(new
                        {
                            status = "Success",
                            message = "Delete succesfully...!!!"
                        });
                    }
                    else
                    {
                        return NotFound(new
                        {
                            status = "Error",
                            message = "Not found",
                        });
                    }
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "Invalid event"

                    });
                }
                
            }catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    status = "Success",
                    message = "Something went wrong...!!!"+ex.Message,
                });
            }
        }
    }
}
