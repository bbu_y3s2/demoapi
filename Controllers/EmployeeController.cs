﻿using DemoAPI.Models.Data;
using DemoAPI.Models.Request;
using DemoAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Net;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        protected readonly DemoDbContext _context; //intial connection
        protected List<Address> _address;
        public EmployeeController(DemoDbContext context)
        {
            _context = context;
            _address = _context.Addresses.ToList();
           
        }

        [HttpPost("/api/addEmployee")]
        public async Task<ActionResult> AddEmployee([FromBody] EmployeeRequest req)
        {
            try
            {
                var empJson = JsonConvert.SerializeObject(req);
                var empObj = JsonConvert.DeserializeObject<Employee>(empJson);

                var empExpReq = (from exp in req.EmployeeExpRequests
                                    select new
                                    {
                                        EmployeeExpId = exp.EmployeeExpId,
                                        EmployeeId = req.EmployeeId,
                                        Position = exp.Position,
                                        Salary = exp.Salary,
                                        DateJoin = exp.DateJoin,
                                        DateResign = exp.DateResign
                                    }).ToList();
                var empExpJson = JsonConvert.SerializeObject(empExpReq);
                var empExpObj = JsonConvert.DeserializeObject<List<EmployeeExp>>(empExpJson);

                var empEduReq = (from edu in req.EmployeeEduRequests
                                 select new
                                 {
                                     EmployeeEduId = edu.EmployeeEduId,
                                     EmployeeId = req.EmployeeId,
                                     EducationLevelId = edu.EducationLevelId,
                                     MajorId = edu.MajorId,
                                     SchoolName = edu.SchoolName,
                                     YearStart = edu.YearStart,
                                     YearEnd = edu.YearEnd,
                                 }).ToList();
                var empEduJson = JsonConvert.SerializeObject(empEduReq);
                var empEduObj = JsonConvert.DeserializeObject<List<EmployeeEdu>>(empEduJson);

                await _context.Employees.AddAsync(empObj);
                await _context.SaveChangesAsync();

                return Ok(new
                {
                    status = "Success",
                    message = "Your transaction is completed! ",
                    empObj = empObj,
                    empExperObj = empExpObj,
                    empEduObj = empEduObj
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message = "Someting when wrong! " + ex.Message
                });
            }
        }

        [HttpGet("/api/getEmployeeById/{employeeId}")]
        public async Task<ActionResult> GetEmployeeByIdAsync(string employeeId)
        {
            try
            {
                var emp = await (from e in _context.Employees
                                 where e.EmployeeId == employeeId
                                 select new
                                 {
                                     e,
                                     experience = _context.EmployeeExps.Where(x => x.EmployeeId == employeeId).ToList(),
                                     education = _context.EmployeeEdus.Where(x => x.EmployeeId == employeeId).ToList(),

                                 }).FirstOrDefaultAsync();

                if (emp != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = "Employee details retrieved successfully",
                        employee = emp
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = "Error",
                        message = "Employee not found!"
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new
                {
                    status = "Error",
                    message = ex.Message
                });
            }
        }


        [HttpGet("/api/getEmployeeList")]
        public async Task<ActionResult> GetEmployee()
        {
            var emp = (from employee in await _context.Employees.ToListAsync()
                       select new
                       {
                           EmployeeId = employee.EmployeeId,
                           EmployeeNameKh = employee.EmployeeNameKh,
                           EmployeeNameEn = employee.EmployeeNameEn,
                           Position = employee.Position,
                           Department = employee.Department,
                           address = AddressService.GetAddresses(villageId: employee.Address, _address),
                       }).ToList();
            try
            {
                return Ok(new
                {
                    status = "Success",
                    message = "List of Employee",
                    emp = emp,
                });
            }
            catch (Exception)
            {
                return Ok(new
                {
                    status = "Erorr",
                    message = "Somthing when wrong !",
                });
            }
        }

        [HttpPost("/api/CUDEmployee")]
        public async Task<ActionResult> CUDEmployee([FromBody] EmployeeRequest req)
        {
            try
            {
                
                if (req.CUD == "C")
                {
                    var empJson = JsonConvert.SerializeObject(req);
                    var empObj = JsonConvert.DeserializeObject<Employee>(empJson);

                    var empExpReq = (from exp in req.EmployeeExpRequests
                                     select new
                                     {
                                         EmployeeExpId = exp.EmployeeExpId,
                                         EmployeeId = req.EmployeeId,
                                         Position = exp.Position,
                                         Salary = exp.Salary,
                                         DateJoin = exp.DateJoin,
                                         DateResign = exp.DateResign
                                     }).ToList();
                    var empExpJson = JsonConvert.SerializeObject(empExpReq);
                    var empExpObj = JsonConvert.DeserializeObject<List<EmployeeExp>>(empExpJson);

                    var empEduReq = (from edu in req.EmployeeEduRequests
                                     select new
                                     {
                                         EmployeeEduId = edu.EmployeeEduId,
                                         EmployeeId = req.EmployeeId,
                                         EducationLevelId = edu.EducationLevelId,
                                         MajorId = edu.MajorId,
                                         SchoolName = edu.SchoolName,
                                         YearStart = edu.YearStart,
                                         YearEnd = edu.YearEnd,
                                     }).ToList();
                    var empEduJson = JsonConvert.SerializeObject(empEduReq);
                    var empEduObj = JsonConvert.DeserializeObject<List<EmployeeEdu>>(empEduJson);
                    await _context.Employees.AddAsync(empObj);
                    await _context.SaveChangesAsync();

                    return Ok(new
                    {
                        status = "Success",
                        message = "Add new employee successfully...!!! ",
                        empObj = empObj,
                        empExperObj = empExpObj,
                        empEduObj = empEduObj
                    });
                }
                else if (req.CUD == "U")
                {
                    
                    var existedEmployee = await _context.Employees.Where(x => x.EmployeeId == req.EmployeeId).FirstOrDefaultAsync();
                    if(existedEmployee != null)
                    {
                        
                        _context.ChangeTracker.Clear();
                        
                        existedEmployee.EmployeeNameEn = req.EmployeeNameEn;
                        existedEmployee.EmployeeNameKh = req.EmployeeNameKh;
                        existedEmployee.Position = req.Position;
                        existedEmployee.Address = req.Address;

                        _context.Employees.Update(existedEmployee);
                        await _context.SaveChangesAsync();
                    }
                    return Ok(new
                    {
                        status = "Success",
                        message = "Update employee successfully...!!!",
                    });
                }else if(req.CUD == "D")
                {
                    var existedEmployee = await _context.Employees.Where(x => x.EmployeeId == req.EmployeeId).FirstOrDefaultAsync();
                    if (existedEmployee != null)
                    {
                        _context.ChangeTracker.Clear();
                        _context.Employees.Remove(existedEmployee);
                        _context.SaveChangesAsync();
                    }
                    return Ok(new
                    {
                        status = "Success",
                        message = "Delete successfully...!!!"
                    });
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "Invalid event"
                    });
                }
                
            }
            catch(Exception ex)
            {
                return StatusCode(500, new
                {
                    status = "Error",
                    message = ex.Message,
                });
            }
        }


    }
}
