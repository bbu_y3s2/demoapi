﻿using DemoAPI.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly DemoDbContext _context = new DemoDbContext();

        public AddressController(DemoDbContext context)
        {
            _context = context;
        }

        //[HttpGet("/api/AddressList")]
        //public async Task<ActionResult> GetAddress()
        //{
        //    try
        //    {
        //        var addressList = await _context.Addresses.ToListAsync();
        //        return Ok(new
        //        {
        //            status = "Success",
        //            message = "",
        //            addressList = addressList
        //        });
        //    }catch (Exception ex) 
        //    {
        //        return Ok(new
        //        {
        //            status = "Error",
        //            message = "Something went wront...!!!" + ex.Message.ToString()
        //        });
        //    }
        //}


        [HttpPost("/api/getAddress")]
        public async Task<ActionResult> GetAddress ([FromBody] AddressRequest request)
        {
            try
            {
                var addressList = await _context.Addresses.Where(x => x.ParentId == request.ParentId).ToListAsync();
                if(addressList.FirstOrDefault() != null)
                {
                    return Ok(new
                    {
                        status = "Success",
                        message = " ",
                        address = addressList
                    });
                }
                else
                {
                    return Ok(new
                    {
                        status = "Error",
                        message = "No data",
                        address = addressList
                    });
                }
                
            }
            catch(Exception ex) 
            {
                return Ok(new
                {
                    status = "Error",
                    message = ex.Message
                });
            }
            
        }
    }
}
