﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class EducationLevelController : ControllerBase
    {
        private readonly DemoDbContext _context = new DemoDbContext();
        public EducationLevelController(DemoDbContext context)
        {
            context = _context;
        }

        [HttpGet("/api/getEducationLevel")]
        public async Task<ActionResult> GetEducationLevel()
        {
            try
            {
                var edu = await _context.EducationLevels.ToListAsync();

                return Ok(new
                {
                    status = "Success",
                    message = "List of Degree",
                    educationLevel = edu
                });

            }catch (Exception ex)
            {
                return Ok(new
                {
                    status = "Error",
                    message = ex.Message
                });
            }
        }
    }
}
