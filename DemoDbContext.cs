﻿using DemoAPI.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace DemoAPI
{
    public class DemoDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder opt)
        {
            base.OnConfiguring(opt);
            opt.UseOracle(@"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                            (HOST=localhost)(PORT=1522))(CONNECT_DATA
                            =(SERVICE_NAME=xe)))
                            ;User Id=phai;Password=welcome123;");
        }
        public DbSet<Department>Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeEdu> EmployeeEdus { get; set; }
        public DbSet<EmployeeExp> EmployeeExps { get; set; }
        public DbSet<Major> Majors { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
    }
}
