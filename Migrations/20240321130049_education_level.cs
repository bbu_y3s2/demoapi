﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DemoAPI.Migrations
{
    /// <inheritdoc />
    public partial class education_level : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Degree",
                table: "EmployeeEdus");

            migrationBuilder.DropColumn(
                name: "Major",
                table: "EmployeeEdus");

            migrationBuilder.AddColumn<int>(
                name: "EducationLevelId",
                table: "EmployeeEdus",
                type: "NUMBER(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MajorId",
                table: "EmployeeEdus",
                type: "NUMBER(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EducationLevels",
                columns: table => new
                {
                    EducationLevelId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    EducationLevelName = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationLevels", x => x.EducationLevelId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EducationLevels");

            migrationBuilder.DropColumn(
                name: "EducationLevelId",
                table: "EmployeeEdus");

            migrationBuilder.DropColumn(
                name: "MajorId",
                table: "EmployeeEdus");

            migrationBuilder.AddColumn<string>(
                name: "Degree",
                table: "EmployeeEdus",
                type: "NVARCHAR2(2000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Major",
                table: "EmployeeEdus",
                type: "NVARCHAR2(2000)",
                nullable: true);
        }
    }
}
