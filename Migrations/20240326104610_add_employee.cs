﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DemoAPI.Migrations
{
    /// <inheritdoc />
    public partial class add_employee : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Position",
                table: "EmployeeExps");

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "EmployeeExps",
                type: "NUMBER(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<string>(type: "NVARCHAR2(450)", nullable: false),
                    EmployeeNameKh = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    EmployeeNameEn = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Gender = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Position = table.Column<int>(type: "NVARCHAR2(2000)", nullable: false),
                    Department = table.Column<int>(type: "NVARCHAR2(2000)", nullable: false),
                    Address = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CreateBy = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "EmployeeExps");

            migrationBuilder.AddColumn<string>(
                name: "Position",
                table: "EmployeeExps",
                type: "NVARCHAR2(2000)",
                nullable: true);
        }
    }
}
