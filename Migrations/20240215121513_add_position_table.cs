﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DemoAPI.Migrations
{
    /// <inheritdoc />
    public partial class add_position_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    PositionId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    PositionNameKh = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    PositionNameEn = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    IsActive = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    CreateBy = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CUD = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    LastUpdateBy = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.PositionId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Positions");
        }
    }
}
