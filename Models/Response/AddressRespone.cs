﻿namespace DemoAPI.Models.Response
{
    public class AddressRespone
    {
        public string ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string CommuneId { get; set; }
        public string CommuneName { get; set; }
        public string VillageId { get; set; }
        public string VillageName { get; set; }
    }
}
