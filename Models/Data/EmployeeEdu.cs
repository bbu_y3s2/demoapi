﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAPI.Models.Data
{
    public class EmployeeEdu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeEduId { get; set; }
        public string? EmployeeId { get; set; }
        public int EducationLevelId { get; set; }
        public int MajorId { get; set; }
        public string? SchoolName { get; set; }
        public string? YearStart { get; set; }
        public string? YearEnd { get;set; }
        
    }
}
