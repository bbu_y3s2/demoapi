﻿using System.ComponentModel.DataAnnotations;

namespace DemoAPI.Models.Data
{
    public class Major
    {
        [Key]
        public int MajorId { get; set; }
        public string? MajorName { get; set; }
    }
}
