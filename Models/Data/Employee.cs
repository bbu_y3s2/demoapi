﻿using System.ComponentModel.DataAnnotations;

namespace DemoAPI.Models.Data
{
    public class Employee
    {
        [Key]
        public string? EmployeeId { get;set; }
        public string? EmployeeNameKh { get; set; }
        public string? EmployeeNameEn { get; set; }
        public string? Gender { get; set; }
        public string? Position { get; set; }
        public string? Department { get; set; }
        public string? Address { get; set; }
        public string? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? CUD { get; set; }
        
    }
}
