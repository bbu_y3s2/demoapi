﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoAPI.Models.Data
{
    public class EmployeeExp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeExpId { get; set; }
        public string? EmployeeId { get; set; }
        public int PositionId { get; set; }
        public string? Salary { get; set; }
        public DateTime? DateJoin { get; set; }
        public DateTime? DateResign { get; set; }
    }
}
