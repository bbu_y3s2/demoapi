﻿namespace DemoAPI.Models.Request
{
    public class EmployeeEduRequest
    {
        public int EmployeeEduId { get; set; }
        public int EducationLevelId { get; set; }
        public int MajorId { get; set; }
        public string? SchoolName { get; set; }
        public string? YearStart { get; set; }
        public string? YearEnd { get; set; }
    }
}
