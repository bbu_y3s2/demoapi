﻿namespace DemoAPI.Models.Request
{
    public class EmployeeRequest
    {
        public string? EmployeeId { get; set; }
        public string? EmployeeNameKh { get; set; }
        public string? EmployeeNameEn { get; set; }
        public string? Gender { get; set; }
        public string? Position { get; set; }
        public string? Department { get; set; }
        public string? Address { get; set; }
        public string? CreateBy { get; set; }
        public string? CUD { get; set; }

        public List<EmployeeExpRequest>? EmployeeExpRequests { get; set; }
        public List<EmployeeEduRequest>? EmployeeEduRequests { get; set; }
        public List<MajorResquest>? MajorResquests { get; set; }
    }
}
