﻿namespace DemoAPI.Models.Request
{
    public class MajorResquest
    {
        public int MajorId {  get; set; }
        public string? MajorName { get; set; }
    }
}
