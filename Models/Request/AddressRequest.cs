﻿namespace DemoAPI.Models.Request
{
    public class AddressRequest
    {
        public string? Id { get; set; }
        public string? NameKh { get; set; }
        public string? NameEn { get; set; }
        public string? ParentId { get; set; }
        
    }
}
